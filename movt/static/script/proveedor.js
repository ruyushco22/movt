$(document).ready(function() {
  $("#frm_nuevo_proveedor").validate({
      errorPlacement: function(error, element) {
        var fieldsWithCustomPlacement = ["ruc", "direccion", "nombre", "mail", "telefono","año"];
        if (fieldsWithCustomPlacement.includes(element.attr("name"))) {
              // Insertar el mensaje de error después del elemento padre del campo
              error.insertAfter(element.parent());
              error.addClass("text-danger");
          } else {
              // Agregar la clase "text-danger" al mensaje de error
              error.addClass("text-danger");
              // Insertar el mensaje de error después del elemento
              error.insertAfter(element);
          }
      },
      rules: {
          "nombre": {
              required: true,
              letras: true
          },
          "telefono": {
              required: true,
              minlength: 10,
              maxlength: 10
          },
          "mail": {
              required: true,
              email: true
          },
          "ruc": {
              required: true,
              minlength: 13,
              maxlength: 13
          },
          "direccion": {
              required: true,
          },
          "año": {
              required: true,
              minlength: 4,
              maxlength: 4
          }
      },
      messages: {
          "nombre": {
              required: "Ingrese el nombre del proveedor"
          },
          "telefono": {
              required: "Ingrese un número de teléfono válido",
              minlength: "El número de teléfono debe tener 10 dígitos",
              maxlength: "Solo debe tener 10 dígitos"
          },
          "mail": {
              required: "Ingrese una dirección de correo electrónico válida",
              email: "El email debe ser válido(@gmail.com)"
          },
          "ruc": {
              required: "Ingrese un RUC válido",
              minlength: "El RUC debe tener 13 dígitos",
              maxlength: "El RUC debe tener 13 dígitos"
          },
          "direccion": {
              required: "Ingrese la dirección del proveedor"
          },
          "año": {
              required: "Ingrese un año válido",
              minlength: "El año debe tener 4 dígitos",
              maxlength: "El año debe tener 4 dígitos"
          }
      }
  });
  $.validator.addMethod("letras", function(value, element) {
    return this.optional(element) || /^[a-zA-Z\sáéíóúÁÉÍÓÚüÜñÑ]+$/.test(value);
  }, "Solo se permiten letras");
});
