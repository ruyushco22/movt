$(document).ready(function() {
    $("#frm_nuevo_usuario").validate({
      errorPlacement: function(error, element) {
        var fieldsWithCustomPlacement = ["nombre", "apellido", "telefono", "mail", "cedula", "direccion", "edad", "user"];
        if (fieldsWithCustomPlacement.includes(element.attr("name"))) {
          // Insertar el mensaje de error después del elemento padre del campo
          error.insertAfter(element.parent());
          error.addClass("text-danger");
        } else {
          // Agregar la clase "text-danger" al mensaje de error
          error.addClass("text-danger");
          // Insertar el mensaje de error después del elemento
          error.insertAfter(element);
        }
      },
      rules: {
        "nombre": {
          required: true,
          letras: true
        },
        "apellido": {
          required: true,
          letras: true
        },
        "telefono": {
          required: true,
          minlength: 10,
          maxlength: 10
        },
        "mail": {
          required: true,
          email: true
        },
        "cedula": {
          required: true,
          minlength: 10,
          maxlength: 13
        },
        "direccion": {
          required: true
        },
        "edad": {
          required: true,
          minlength: 2,
          maxlength: 2
        }
      },
      messages: {
        "nombre": {
          required: "Digite su nombre por favor"
        },
        "apellido": {
          required: "Digite su apellido por favor"
        },
        "telefono": {
          required: "Digite su número telefónico por favor",
          minlength: "El número de teléfono debe tener 10 dígitos",
          maxlength: "El número de teléfono debe tener 10 dígitos"
        },
        "mail": {
          required: "Ingrese una dirección de correo electrónico válida",
          email: "El email debe ser válido (@gmail.com)"
        },
        "cedula": {
          required: "Ingrese su número de cédula por favor",
          minlength: "La cédula debe tener al menos 10 dígitos",
          maxlength: "La cédula no debe exceder los 13 dígitos"
        },
        "direccion": {
          required: "Ingrese su dirección por favor"
        },
        "edad": {
          required: "Ingrese su edad por favor",
          minlength: "La edad debe tener 2 dígitos",
          maxlength: "La edad debe tener 2 dígitos"
        }
      }
    });

    $.validator.addMethod("letras", function(value, element) {
      return this.optional(element) || /^[a-zA-Z\sáéíóúÁÉÍÓÚüÜñÑ]+$/.test(value);
    }, "Solo se permiten letras");
  });
