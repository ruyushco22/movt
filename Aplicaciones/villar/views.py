from django.shortcuts import render, redirect
from .models import  Proveedor,Cliente,Usuario
from django.conf import settings
from django.http import JsonResponse


from django.contrib import messages

# Create your views here.
def buscar_cargo(request):
    if request.is_ajax():
        query = request.GET.get('query', '')
        # Realiza la búsqueda en tu modelo
        resultados = Usuario.objects.filter(nombre__icontains=query)
        # Convierte los resultados en una lista de cadenas
        resultados_lista = [resultado.nombre for resultado in resultados]
        return JsonResponse({'resultados': resultados_lista})
    return JsonResponse({}, status=400)
def inicio(request):
    return render (request,'inicio.html')

def prueba(request):
    return render (request,'prueba.html')

def dibujos(request):
    return render (request,'dibujos.html')



def proveedor(request):
    proveedorBdd=Proveedor.objects.all()
    return render (request,'proveedor.html',{'proveedor':proveedorBdd})

def guardarProveedor(request):
    nombre=request.POST["nombre"]
    telefono=request.POST["telefono"]
    mail=request.POST["mail"]
    ruc=request.POST["ruc"]
    direccion=request.POST["direccion"]
    año=request.POST["año"]
    logo=request.FILES.get("logo")


    nuevoProveedor=Proveedor.objects.create(
        nombre=nombre,
        telefono=telefono,
        mail=mail,
        ruc=ruc,
        direccion=direccion,
        año=año,
        logo=logo
        )
    messages.success(request,'Proveedor Guardado Exitosamente')
    return redirect('/proveedor/')

def eliminarProveedor(request,id):
    proveedorEliminar=Proveedor.objects.get(id=id)
    proveedorEliminar.delete()
    messages.success(request,'Proveedor Borrado Exitosamente')
    return redirect ('/proveedor/')



def editarProveedor(request, id):
    proveedorEditar = Proveedor.objects.get(id=id)
    return render (request, 'proveedorEditar.html',{'proveedor': proveedorEditar})



def procesarActualizacionProveedor(request):
    id=request.POST["id"]
    nombre=request.POST["nombre"]
    telefono=request.POST["telefono"]
    mail=request.POST["mail"]
    ruc=request.POST["ruc"]
    direccion=request.POST["direccion"]
    año=request.POST["año"]
    logo=request.FILES.get("logo")

    #Insertando datos mediante el ORM de DJANGO
    proveedorEditar=Proveedor.objects.get(id=id)
    proveedorEditar.nombre=nombre
    proveedorEditar.telefono=telefono
    proveedorEditar.mail=mail
    proveedorEditar.ruc=ruc
    proveedorEditar.direccion=direccion
    proveedorEditar.año=año
    if logo is not None:
        proveedorEditar.logo=logo
    proveedorEditar.save()
    messages.success(request,
      'Proveedor actualizada Exitosamente')
    return redirect('/proveedor/')






#CLIENTES


def cliente(request):
    clienteBdd=Cliente.objects.all()
    return render (request,'cliente.html',{'cliente':clienteBdd})

def guardarCliente(request):
    nombre=request.POST["nombre"]
    apellido=request.POST["apellido"]
    telefono=request.POST["telefono"]
    mail=request.POST["mail"]
    cedula=request.POST["cedula"]
    direccion=request.POST["direccion"]
    nuevoCliente=Cliente.objects.create(
        nombre=nombre,
        apellido=apellido,
        telefono=telefono,
        mail=mail,
        cedula=cedula,
        direccion=direccion,
        )
    messages.success(request,'Cliente Guardado Exitosamente')
    return redirect('/cliente/')

def eliminarCliente(request,id):
    clienteEliminar=Cliente.objects.get(id=id)
    clienteEliminar.delete()
    messages.success(request,'Cliente Borrado Exitosamente')
    return redirect ('/cliente/')



def editarCliente(request, id):
    clienteEditar = Cliente.objects.get(id=id)
    return render (request, 'clienteEditar.html',{'cliente': clienteEditar})



def procesarActualizacionCliente(request):
    id=request.POST["id"]
    nombre=request.POST["nombre"]
    apellido=request.POST["apellido"]
    telefono=request.POST["telefono"]
    mail=request.POST["mail"]
    cedula=request.POST["cedula"]
    direccion=request.POST["direccion"]

    #Insertando datos mediante el ORM de DJANGO
    clienteEditar=Cliente.objects.get(id=id)
    clienteEditar.nombre=nombre
    clienteEditar.apellido=apellido
    clienteEditar.telefono=telefono
    clienteEditar.mail=mail
    clienteEditar.cedula=cedula
    clienteEditar.direccion=direccion
    clienteEditar.save()
    messages.success(request,
      'Cliente actualizada Exitosamente')
    return redirect('/cliente/')











#USUARIOS


def usuario(request):
    usuarioBdd=Usuario.objects.all()
    return render (request,'usuario.html',{'usuario':usuarioBdd})

def guardarUsuario(request):
    nombre=request.POST["nombre"]
    apellido=request.POST["apellido"]
    telefono=request.POST["telefono"]
    mail=request.POST["mail"]
    cedula=request.POST["cedula"]
    direccion=request.POST["direccion"]
    edad=request.POST["edad"]

    user=request.FILES.get("user")
    nuevoUsuario=Usuario.objects.create(
        nombre=nombre,
        apellido=apellido,
        telefono=telefono,
        mail=mail,
        cedula=cedula,
        direccion=direccion,
        edad=edad,
        user=user,
        )
    messages.success(request,'Usuario Guardado Exitosamente')
    return redirect('/usuario/')

def eliminarUsuario(request,id):
    usuarioEliminar=Usuario.objects.get(id=id)
    usuarioEliminar.delete()
    messages.success(request,'Usuario Borrado Exitosamente')
    return redirect ('/usuario/')



def editarUsuario(request, id):
    usuarioEditar = Usuario.objects.get(id=id)
    return render (request, 'usuarioEditar.html',{'usuario': usuarioEditar})



def procesarActualizacionUsuario(request):
    id=request.POST["id"]
    nombre=request.POST["nombre"]
    apellido=request.POST["apellido"]
    telefono=request.POST["telefono"]
    mail=request.POST["mail"]
    cedula=request.POST["cedula"]
    direccion=request.POST["direccion"]
    edad=request.POST["edad"]

    user=request.FILES.get("user")


    #Insertando datos mediante el ORM de DJANGO
    usuarioEditar=Usuario.objects.get(id=id)
    usuarioEditar.nombre=nombre
    usuarioEditar.apellido=apellido
    usuarioEditar.telefono=telefono
    usuarioEditar.mail=mail
    usuarioEditar.cedula=cedula
    usuarioEditar.direccion=direccion
    usuarioEditar.edad=edad

    if user is not None:
        usuarioEditar.user=user
    usuarioEditar.save()
    messages.success(request,
      'Usuario actualizada Exitosamente')
    return redirect('/usuario/')
