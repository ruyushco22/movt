from django.apps import AppConfig


class VillarConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Aplicaciones.villar'
