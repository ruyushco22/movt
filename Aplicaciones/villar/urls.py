from django.urls import path
from . import views

urlpatterns = [
    path('', views.inicio, name='inicio'),
    path('proveedor/', views.proveedor, name="proveedor"),
    path('guardarProveedor/', views.guardarProveedor, name="guardarProveedor"),
    path('eliminarProveedor/<id>/', views.eliminarProveedor, name="eliminarProveedor"),
    path('editarProveedor/<id>/', views.editarProveedor, name="editarProveedor"),
    path('procesarActualizacionProveedor/', views.procesarActualizacionProveedor, name="procesarActualizacionProveedor"),

    path('buscar-cargo/', views.buscar_cargo, name='buscar_cargo'),

    path('cliente/', views.cliente, name="cliente"),
    path('guardarCliente/', views.guardarCliente, name="guardarCliente"),
    path('eliminarCliente/<id>/', views.eliminarCliente, name="eliminarCliente"),
    path('editarCliente/<id>/', views.editarCliente, name="editarCliente"),
    path('procesarActualizacionCliente/', views.procesarActualizacionCliente, name="procesarActualizacionCliente"),


    path('usuario/', views.usuario, name="usuario"),
    path('guardarUsuario/', views.guardarUsuario, name="guardarUsuario"),
    path('eliminarUsuario/<id>/', views.eliminarUsuario, name="eliminarUsuario"),
    path('editarUsuario/<id>/', views.editarUsuario, name="editarUsuario"),
    path('procesarActualizacionUsuario/', views.procesarActualizacionUsuario, name="procesarActualizacionUsuario"),


    path('prueba', views.prueba, name="prueba"),
    path('dibujos', views.dibujos, name="dibujos")

]
