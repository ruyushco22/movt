# Generated by Django 4.2.7 on 2024-02-07 03:17

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Proveedor',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=150)),
                ('telefono', models.CharField(max_length=20)),
                ('mail', models.EmailField(max_length=254)),
                ('ruc', models.CharField(max_length=13)),
                ('direccion', models.CharField(max_length=255)),
                ('año', models.PositiveIntegerField()),
            ],
        ),
    ]
