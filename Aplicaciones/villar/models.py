from django.db import models

# Create your models here.
class Proveedor(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=150)
    telefono = models.CharField(max_length=20)  # Por ejemplo, asumiendo que el teléfono es una cadena de texto
    mail = models.EmailField(max_length=254)     # Campo para el correo electrónico
    ruc = models.CharField(max_length=13)        # RUC es un número de 13 dígitos en Ecuador
    direccion = models.CharField(max_length=255) # Dirección del proveedor
    año = models.PositiveIntegerField()
    logo=models.FileField(upload_to='logos', null=True, blank=True)

    def __str__(self):
        fila="{0}: {1} {2} - {3} -{4} -{5}"
        return fila.format(
            self.nombre,
            self.telefono,
            self.mail,
            self.ruc,
            self.direccion,
            self.año,
            self.logo
            )

class Cliente(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=150)
    apellido = models.CharField(max_length=100, default='')  # Define aquí un valor predeterminado
    telefono = models.CharField(max_length=20)  # Por ejemplo, asumiendo que el teléfono es una cadena de texto
    mail = models.EmailField(max_length=254)     # Campo para el correo electrónico
    cedula = models.CharField(max_length=13)        # RUC es un número de 13 dígitos en Ecuador
    direccion = models.CharField(max_length=255) # Dirección del proveedor

    def __str__(self):
        fila="{0}: {1} {2} - {3} {4}"
        return fila.format(
            self.nombre,
            self.apellido,
            self.telefono,
            self.mail,
            self.cedula,
            self.direccion,

            )

class Usuario(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=150)
    apellido = models.CharField(max_length=150)
    telefono = models.CharField(max_length=20)  # Por ejemplo, asumiendo que el teléfono es una cadena de texto
    mail = models.EmailField(max_length=254)     # Campo para el correo electrónico
    cedula = models.CharField(max_length=13)        # RUC es un número de 13 dígitos en Ecuador
    direccion = models.CharField(max_length=200) # Dirección del proveedor
    edad = models.PositiveIntegerField(null=True, blank=True)
    user=models.FileField(upload_to='users', null=True, blank=True)

    def __str__(self):
        fila="{0}: {1} {2} - {3} -{4} -{5}  {6}"
        return fila.format(
            self.nombre,
            self.apellido,
            self.telefono,
            self.mail,
            self.cedula,
            self.direccion,
            self.edad,
            self.user
            )
